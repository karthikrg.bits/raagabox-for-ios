//
//  RBTableHeaderView.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 12/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBTableHeaderView.h"
#import "RBConsts.h"

@interface RBTableHeaderView()

@property (nonatomic, strong) UILabel     *resultLabel;
@property (nonatomic, strong) UIButton    *playAllButton;

@property (nonatomic, assign) id<RBTableHeaderViewDelegate> delegate;

@end

@implementation RBTableHeaderView

- (id)initWithFrame:(CGRect)frame delegate:(id<RBTableHeaderViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _delegate = delegate;
        
        self.backgroundColor = TINT_COLOR;
        
        _playAllButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 80, 5, 80, self.frame.size.height - 10)];
        [_playAllButton setImage:[UIImage imageNamed:@"playall.png"] forState:UIControlStateNormal];
        [_playAllButton addTarget:self action:@selector(playAllButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        _playAllButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self addSubview:_playAllButton];
        
        _resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 150, self.frame.size.height - 10)];
        _resultLabel.backgroundColor = [UIColor clearColor];
        _resultLabel.textColor = [UIColor whiteColor];
        _resultLabel.font = [UIFont boldSystemFontOfSize:12];
        _resultLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self addSubview:_resultLabel];
    }
    
    return self;
}

- (void) setResultCount:(NSUInteger)resultCount
{
    _resultLabel.text = [NSString stringWithFormat:@"%d results", resultCount];
    _playAllButton.hidden = (resultCount == 0);
}

- (void) playAllButtonPressed
{
    [self.delegate playAllButtonPressed];
}

@end

