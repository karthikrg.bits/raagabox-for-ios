//
//  RBAudioPlayerView.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBAudioPlayerView.h"
#import <AVFoundation/AVFoundation.h>

@interface  RBAudioPlayerView()

@property (nonatomic, strong) UIButton *prevButton;
@property (nonatomic, strong) UIButton *playPauseButton;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) UIButton *repeatButton;
@property (nonatomic, strong) UILabel  *startTimeLabel;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel  *endTimeLabel;
@property (nonatomic, strong) UILabel  *currentTimeLabel;

@property (nonatomic, strong) NSURL *currentURL;
@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, assign) BOOL isRepeatEnabled;

@property (nonatomic, strong) AVPlayer *audioPlayer;
@property (nonatomic, strong) NSTimer *progressUpdateTimer;

@property (nonatomic, assign) id<RBAudioPlayerViewDataSource> dataSource;

@end

@implementation RBAudioPlayerView

- (id) initWithFrame:(CGRect)frame dataSource:(id<RBAudioPlayerViewDataSource>)dataSource
{
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    _dataSource = dataSource;
    
    self.backgroundColor = [UIColor colorWithRed:23/255.0f green:186/255.0f blue:255/255.0f alpha:1.0f];
    
    _prevButton = [[UIButton alloc] initWithFrame:CGRectMake((self.frame.size.width - 160)/2, 5, 40, 40)];
    _prevButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [_prevButton setImage:[UIImage imageNamed:@"player_prev.png"] forState:UIControlStateNormal];
    [self addSubview:_prevButton];
    [_prevButton addTarget:self action:@selector(prev) forControlEvents:UIControlEventTouchUpInside];
    
    _playPauseButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_prevButton.frame) + 5, 10, 40, 30)];
    _playPauseButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [_playPauseButton setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
    [self addSubview:_playPauseButton];
    [_playPauseButton addTarget:self action:@selector(playOrPause) forControlEvents:UIControlEventTouchUpInside];
    _isPlaying = NO;
    
    _nextButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_playPauseButton.frame) + 5, 5, 40, 40)];
    _nextButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [_nextButton setImage:[UIImage imageNamed:@"player_next.png"] forState:UIControlStateNormal];
    [self addSubview:_nextButton];
    [_nextButton addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    
    _repeatButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_nextButton.frame) + 5, 5, 40, 40)];
    _repeatButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [_repeatButton setImage:[UIImage imageNamed:@"player_repeat_disabled.png"] forState:UIControlStateNormal];
    [self addSubview:_repeatButton];
    [_repeatButton addTarget:self action:@selector(toggleRepeat) forControlEvents:UIControlEventTouchUpInside];
    
    _startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 320)/2, 35, 50, 50)];
    _startTimeLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    _startTimeLabel.backgroundColor = [UIColor clearColor];
    _startTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    _startTimeLabel.textColor = [UIColor whiteColor];
    _startTimeLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:_startTimeLabel];
    
    _slider = [[UISlider alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_startTimeLabel.frame), 50, 220, 20)];
    _slider.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self addSubview:_slider];
    [_slider addTarget:self action:@selector(slideMoveStart) forControlEvents:UIControlEventTouchDown];
    [_slider addTarget:self action:@selector(slideValueChanged) forControlEvents:UIControlEventValueChanged];
    [_slider addTarget:self action:@selector(slideMoveEnd) forControlEvents:UIControlEventTouchUpInside];
    
    _endTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_slider.frame), 35, 50, 50)];
    _endTimeLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    _endTimeLabel.backgroundColor = [UIColor clearColor];
    _endTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    _endTimeLabel.textColor = [UIColor whiteColor];
    [self addSubview:_endTimeLabel];
    
    _currentTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    _currentTimeLabel.backgroundColor = [UIColor blackColor];
    _currentTimeLabel.textColor = [UIColor whiteColor];
    _currentTimeLabel.textAlignment = NSTextAlignmentCenter;
    _currentTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    _currentTimeLabel.hidden = YES;
    [self addSubview:_currentTimeLabel];
    
    return self;
}

- (void) playOrPause
{
    if (_isPlaying) {
        [self pause];
    }
    else {
        [self play];
    }    
}

- (BOOL) isPlaying
{
    return _isPlaying;
}

- (BOOL) isRepeatEnabled
{
    return _isRepeatEnabled;
}

- (void) updateTime
{
    if (_audioPlayer.currentItem == nil) return;
    
     _slider.value =  CMTimeGetSeconds(_audioPlayer.currentItem.currentTime);
    _startTimeLabel.text = [self getTimeLabelTextFromDuration:CMTimeGetSeconds(_audioPlayer.currentItem.currentTime)];
    _endTimeLabel.text = [NSString stringWithFormat:@"-%@",
    [self getTimeLabelTextFromDuration:(CMTimeGetSeconds(_audioPlayer.currentItem.duration) - CMTimeGetSeconds(_audioPlayer.currentItem.currentTime))]];
}

- (void) slideMoveStart
{
    [_progressUpdateTimer invalidate];
    [_currentTimeLabel setHidden:NO];
}

- (void) slideValueChanged
{
    if ([_currentTimeLabel isHidden]) return;
    
    CGRect trackRect = [self.slider trackRectForBounds:self.slider.bounds];
    CGRect thumbRect = [self.slider thumbRectForBounds:self.slider.bounds
                                             trackRect:trackRect
                                                 value:self.slider.value];
    
    _currentTimeLabel.center =
        CGPointMake(thumbRect.origin.x + self.slider.frame.origin.x,  self.slider.frame.origin.y - 20);
    _currentTimeLabel.text = [self getTimeLabelTextFromDuration:_slider.value];
}

- (void) slideMoveEnd
{
    [_currentTimeLabel setHidden:YES];
    [_audioPlayer.currentItem seekToTime:CMTimeMakeWithSeconds(_slider.value, 1)];
    [self scheduleProgressUpdateTimer];
}

- (void) play
{
    if (_audioPlayer == nil) {
        
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:[self getCurrentTrackURL]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        _audioPlayer = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    }
    
    _slider.minimumValue = 0;
    _slider.maximumValue = CMTimeGetSeconds(_audioPlayer.currentItem.duration);
    
    [self updateTime];
    
    [self performSelectorOnMainThread:@selector(scheduleProgressUpdateTimer) withObject:nil waitUntilDone:NO];
    
    [_playPauseButton setImage:[UIImage imageNamed:@"player_pause.png"] forState:UIControlStateNormal];
    [_audioPlayer play];
    _isPlaying = YES;
}

- (void) scheduleProgressUpdateTimer
{
    _progressUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime)
                                   userInfo:nil repeats:YES];
}

- (void) pause
{
    [_audioPlayer pause];
    [_progressUpdateTimer invalidate];
    
    [_playPauseButton setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
    _isPlaying = NO;
}

- (void) itemDidFinishPlaying
{
    [self itemFinishedPlayingCleanup];
    
    if ([_dataSource isPlayAllEnabled]) {
        [self next];
    }
    else if (_isRepeatEnabled) {
        [self play];
    }
}

- (void) itemFinishedPlayingCleanup
{
    _audioPlayer = nil;
    [_progressUpdateTimer invalidate];
    _slider.value = 0;
    _startTimeLabel.text = @"";
    _endTimeLabel.text = @"";
    
    [_playPauseButton setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
    _isPlaying = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) prev
{
    [self.dataSource moveToPrev];
}

- (void) next
{
    [self.dataSource moveToNext];
}

- (void) stop
{
    if (_audioPlayer) {
        [_audioPlayer pause];
        [self itemFinishedPlayingCleanup];
    }
}

- (void) toggleRepeat
{
    if (_isRepeatEnabled) {
       [_repeatButton setImage:[UIImage imageNamed:@"player_repeat_disabled.png"] forState:UIControlStateNormal]; 
    }
    else {
       [_repeatButton setImage:[UIImage imageNamed:@"player_repeat.png"] forState:UIControlStateNormal];
    }
    
    _isRepeatEnabled = !_isRepeatEnabled;
}

#pragma mark Enable/disable controls

- (void) disableControls
{
    [_prevButton setEnabled:NO];
    [_playPauseButton setEnabled:NO];
    [_nextButton setEnabled:NO];
    [_repeatButton setEnabled:NO];
    [_slider setEnabled:NO];
}

- (void) enableControls
{
    [_prevButton setEnabled:YES];
    [_playPauseButton setEnabled:YES];
    [_nextButton setEnabled:YES];
    [_repeatButton setEnabled:YES];
    [_slider setEnabled:YES];
}

#pragma mark Private methods

- (NSURL *) getCurrentTrackURL
{
    NSString *filePath = [self.dataSource getPlayFilePath];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",@"http://raagabox.com/media/music/", filePath];
    
    return [NSURL URLWithString:urlStr];
}

- (NSString*) getTimeLabelTextFromDuration:(Float64)seconds
{
    int hours = 0, mins = 0, secs = 0;
    
    if (seconds > 3600) {
        hours = seconds/3600;
        
        int remainingSeconds = (int)seconds % 3600;
        mins = remainingSeconds / 60;
        secs = remainingSeconds % 60;
        
        return [NSString stringWithFormat:@"%d:%02d:%02d", hours, mins, secs];
    }
    else {
        mins = seconds / 60;
        secs = (int)seconds % 60;
        
        return [NSString stringWithFormat:@"%d:%02d", mins, secs];
    }
}

@end
