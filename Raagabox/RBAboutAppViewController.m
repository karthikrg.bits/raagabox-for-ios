//
//  RBAboutAppViewController.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBAboutAppViewController.h"
#import "RBConsts.h"

@implementation RBAboutAppViewController

- (void) viewDidLoad
{
    self.title = @"About Raagabox";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 200)/2, (self.view.frame.size.height - 40)/2, 200, 40)];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"About app";
    label.textColor = TINT_COLOR;
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:label];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (void) doneButtonPressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
