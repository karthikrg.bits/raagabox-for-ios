//
//  RBSearchEntry.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 02/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBSearchEntry.h"

@interface RBSearchEntry()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *filePath;

@end

@implementation RBSearchEntry

- (id) initWithSearchEntry:(NSString*)entry
{
    NSArray *parts = [entry componentsSeparatedByString:@"','"];
    if ([parts count] != 2) {
        NSLog(@"Error parsing search entry %@; count = %d", entry, [parts count]);
        return nil;
    }
    
    NSString *title    = [parts[0] substringFromIndex:1];
    NSString *filePath = [parts[1] substringToIndex:([parts[1] length] - 1)];
    
    return [self initWithTitle:title filePath:filePath];
}

- (id) initWithTitle:(NSString*)title filePath:(NSString*)filePath
{
    self = [super init];
    if (self) {
        _title = title;
        _filePath = filePath;
    }
    
    return self;
}

@end
