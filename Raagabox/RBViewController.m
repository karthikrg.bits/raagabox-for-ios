//
//  RBViewController.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBViewController.h"
#import "RBConsts.h"
#import "RBAboutAppViewController.h"
#import "RBMainDataSource.h"
#import "RBFavoritesDataSource.h"

@interface RBViewController ()

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) RBMainDataSource *mainDataSource;
@property (nonatomic, strong) RBFavoritesDataSource *favoritesDataSource;
@property (nonatomic, strong) UITableView *bhajansListView;
@property (nonatomic, strong) RBTableHeaderView *headerView;
@property (nonatomic, strong) RBAudioPlayerView *audioPlayer;
@property (nonatomic, strong) UIBarButtonItem *optionsButton;

@property (nonatomic, assign) BOOL isInFavoritesMode;
@property (nonatomic, assign) BOOL isPlayAllOn;

@property (nonatomic, strong) NSIndexPath *currentSelectedIndexPath;

@end

@implementation RBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Raagabox";
    _isInFavoritesMode = NO;
    _isPlayAllOn = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    recognizer.direction = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:recognizer];
    
    self.navigationController.navigationBar.tintColor = TINT_COLOR;
    
    _bhajansListView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 80)];
    _bhajansListView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _bhajansListView.backgroundView = nil;
    
    _headerView    = [[RBTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, _bhajansListView.frame.size.width, 40) delegate:self];
    
    _mainDataSource = [[RBMainDataSource alloc] initWithListener:self];
    _bhajansListView.dataSource = _mainDataSource;
    _bhajansListView.delegate   = self;
    
    _favoritesDataSource = [[RBFavoritesDataSource alloc] initWithListener:self];
    
	_audioPlayer = [[RBAudioPlayerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 80) dataSource:self];
    _audioPlayer.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    _searchBar.tintColor = TINT_COLOR;
    _searchBar.delegate = self;
    _bhajansListView.tableHeaderView = _searchBar;
    
    [self.view addSubview:_bhajansListView];
    [self.view addSubview:_audioPlayer];
    
    UIButton* optionsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [optionsButton setFrame:CGRectMake(0, 0, 30, 25)];
    [optionsButton addTarget:self action:@selector(optionButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [optionsButton setImage:[UIImage imageNamed:@"options.png"] forState:UIControlStateNormal];
    _optionsButton = [[UIBarButtonItem alloc] initWithCustomView:optionsButton];
    self.navigationItem.rightBarButtonItem = _optionsButton;
    
    [self onSearchComplete];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Handle swipe

- (void) handleSwipe:(UISwipeGestureRecognizer*)recognizer
{
    if (_isInFavoritesMode) {
        self.title = @"Raagabox";
        _isInFavoritesMode = NO;
        _bhajansListView.dataSource = _mainDataSource;
    }
    else {
        self.title = @"Favorites";
        _isInFavoritesMode = YES;
        _bhajansListView.dataSource = _favoritesDataSource;
    }
    
    _isPlayAllOn = NO;
    [self performSearch];
}

#pragma mark -
#pragma mark Handle showing options

- (void) optionButtonPressed
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"About Raagabox", @"Send Feedback", nil];
    
    [actionSheet showFromBarButtonItem:_optionsButton animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self showAboutPage];
    }
    else if (buttonIndex == 1) {
        [self sendFeedback];
    }
}

- (void) showAboutPage
{
    UINavigationController *navCtrl =
       [[UINavigationController alloc] initWithRootViewController:[[RBAboutAppViewController alloc] init]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navCtrl.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    navCtrl.navigationBar.tintColor = TINT_COLOR;
    
    [self presentViewController:navCtrl animated:YES completion:nil];
}

- (void) sendFeedback
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        controller.navigationBar.tintColor = TINT_COLOR;
        [controller setSubject:@"Feedback"];
        [controller setMessageBody:@" " isHTML:YES];
        [controller setToRecipients:[NSArray arrayWithObjects:@"feedback@raagabox.com",nil]];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email is not setup." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] ;
        [alert show];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma UISearchBarDelegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    //
    // This delay is added here to avoid performing unnecessary URL lookups when user types
    // very fast.
    //
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(performSearch) object:nil];
    
    if (searchText.length < 3) {
        return;
    }
    
    [self performSelector:@selector(performSearch) withObject:searchText afterDelay:0.5];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    [self performSearch];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    [self performSearch];
}

- (void) performSearch
{
    if (_isInFavoritesMode) {
        [self.favoritesDataSource searchForKeyword:_searchBar.text];
    }
    else {
        if (_searchBar.text.length < 3) {
            [self.mainDataSource searchForKeyword:@""];
        }
        else {
            [self.mainDataSource searchForKeyword:_searchBar.text];   
        }
    }
}

#pragma mark -
#pragma mark RBDataSourceListener methods

- (void) onSearchComplete
{
    _isPlayAllOn = NO;
    [_bhajansListView reloadData];
    _currentSelectedIndexPath = nil;
    
    if ([_bhajansListView numberOfRowsInSection:0] == 0) {
        [self.audioPlayer disableControls];
    }
    else {
        [self.audioPlayer enableControls];
    }
}

- (BOOL) isFavorite:(RBSearchEntry*)entry
{
    return [self.favoritesDataSource isFavorite:entry];
}

- (void) addFavorite:(RBSearchEntry *)entry
{
    [self.favoritesDataSource addFavorite:entry];
}

- (void) removeFavorite:(RBSearchEntry *)entry
{
    [self.favoritesDataSource removeFavorite:entry];
}

#pragma mark -
#pragma mark UITableViewDelegate methods

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_searchBar resignFirstResponder];
    
    if (_currentSelectedIndexPath) {
        [tableView deselectRowAtIndexPath:_currentSelectedIndexPath animated:NO];
    }
    
    _currentSelectedIndexPath = indexPath;
    [tableView selectRowAtIndexPath:_currentSelectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    [self performSelectorInBackground:@selector(playSelectedFile) withObject:nil];
}

- (void) playSelectedFile
{
    if (_currentSelectedIndexPath == nil) return;
    
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
    }
    
    [self.audioPlayer play];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([_searchBar isFirstResponder]) {
        [_searchBar resignFirstResponder];
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSUInteger resultCount = [_bhajansListView numberOfRowsInSection:0];
    [_headerView setResultCount:resultCount];
    return _headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

#pragma mark -
#pragma mark RBAudioPlayerViewDataSource methods

- (NSString*) getPlayFilePath
{
    // Nothing is selected.
    if (_currentSelectedIndexPath == nil) return nil;
    
    int index = _currentSelectedIndexPath.row;
    
    RBSearchEntry *searchEntry =
        (_isInFavoritesMode) ? [self.favoritesDataSource entryAtIndex:index] :
                               [self.mainDataSource entryAtIndex:index];
    if (searchEntry == nil) {
        return nil;
    }
    
    return [searchEntry filePath];
}

- (void) moveToNext
{
    if (_currentSelectedIndexPath == nil) return;
    
    int nextIndex = _currentSelectedIndexPath.row + 1;
    int rowCount = [_bhajansListView numberOfRowsInSection:0];
    
    if (nextIndex >= rowCount) {
        if (rowCount > 0 && self.audioPlayer.isRepeatEnabled && _isPlayAllOn) {
            NSIndexPath *firstRow = [NSIndexPath indexPathForRow:0 inSection:0];
            [_bhajansListView selectRowAtIndexPath:firstRow animated:YES scrollPosition:UITableViewScrollPositionTop];
            [self tableView:_bhajansListView didSelectRowAtIndexPath:firstRow];
        }
        else if ([self.audioPlayer isPlaying]) {
            [self.audioPlayer stop];
        }
    }
    else {
        NSIndexPath *nextRow = [NSIndexPath indexPathForRow:nextIndex inSection:0];
        [_bhajansListView selectRowAtIndexPath:nextRow animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self tableView:_bhajansListView didSelectRowAtIndexPath:nextRow];
    }
}

- (void) moveToPrev
{
    if (_currentSelectedIndexPath == nil) return;
    
    int prevIndex = _currentSelectedIndexPath.row - 1;
    int rowCount = [_bhajansListView numberOfRowsInSection:0];
    
    if (prevIndex < 0) {
        if (rowCount > 0 && self.audioPlayer.isRepeatEnabled && _isPlayAllOn) {
            NSIndexPath *lastRow = [NSIndexPath indexPathForRow:(rowCount - 1) inSection:0];
            [_bhajansListView selectRowAtIndexPath:lastRow animated:YES scrollPosition:UITableViewScrollPositionBottom];
            [self tableView:_bhajansListView didSelectRowAtIndexPath:lastRow];
        }
        else if ([self.audioPlayer isPlaying]) {
            [self.audioPlayer stop];
        }
    }
    else {
        NSIndexPath *prevRow = [NSIndexPath indexPathForRow:prevIndex inSection:0];
        [_bhajansListView selectRowAtIndexPath:prevRow animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self tableView:_bhajansListView didSelectRowAtIndexPath:prevRow];
    }
}

- (BOOL) isPlayAllEnabled
{
    return _isPlayAllOn;
}

#pragma mark RBTableHeaderViewDelegate methods

- (void) playAllButtonPressed
{
    _isPlayAllOn = YES;
    int rowCount = [_bhajansListView numberOfRowsInSection:0];
    
    if (![_audioPlayer isPlaying] && rowCount > 0) {
        [self tableView:_bhajansListView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

@end
