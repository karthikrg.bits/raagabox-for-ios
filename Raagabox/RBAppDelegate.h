//
//  RBAppDelegate.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController *viewController;

@end
