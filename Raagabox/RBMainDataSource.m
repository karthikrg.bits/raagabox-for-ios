//
//  RBMainDataSource.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 02/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBMainDataSource.h"
#import "resolv.h"

@interface RBMainDataSource()

@property (nonatomic, assign) id<RBDataSourceListener> listener;
@property (nonatomic, strong) NSArray                  *searchResults;

@end

@implementation RBMainDataSource

const static NSString *BASE_SEARCH_URL = @"http://www.raagabox.com/api/?searchterm=";

# pragma mark Initialization

- (id) initWithListener:(id<RBDataSourceListener>)listener
{
    self = [super init];
    if (self) {
        _listener      = listener;
        _searchResults = [NSArray array];
    }
    
    return self;
}

#pragma mark Search methods

- (void) searchForKeyword:(NSString*)keyword
{
    // Handle empty or nil strings.
    if (keyword == nil || keyword.length == 0) {
        _searchResults = [NSArray array];
        [self performSelectorOnMainThread:@selector(onSearchComplete) withObject:nil waitUntilDone:NO];
        return;
    }
    
    NSArray *keywordParts = [keyword componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *searchString = [keywordParts componentsJoinedByString:@"+"];
    
    NSURL *searchURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", BASE_SEARCH_URL, searchString]];
    
    // Search in background thread to avoid blocking UI.
    [self performSelectorInBackground:@selector(doSearch:) withObject:searchURL];
}

- (void) doSearch:(NSURL*)searchURL
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:searchURL
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:30];
    // Fetch the response
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    // Make synchronous request
    urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                    returningResponse:&response
                                                error:&error];
    
    if (error) {
        NSLog(@"Error in making request to URL %@ : %@", searchURL, error);
        return;
    }
    
    NSString *responseString = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
    
    NSData *decodedData = nil;
    
    NSData *dataToDecode = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSUInteger decodedBufferLength = dataToDecode.length * 3 / 4;
    uint8_t* decodedBuffer = malloc(decodedBufferLength);
    
    int decodedBufferRealLength = b64_pton(dataToDecode.bytes,
                                           decodedBuffer,
                                           decodedBufferLength);
    if(decodedBufferRealLength >= 0) {
        decodedData = [NSData dataWithBytesNoCopy:decodedBuffer
                                           length:decodedBufferRealLength
                                     freeWhenDone:YES];
    } else {
        free(decodedBuffer);
    }
    
    NSString *convertedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    
    [self parseSearchResponseAndReload:convertedString];
}

- (void) parseSearchResponseAndReload:(NSString*)responseString
{
    if (![responseString hasPrefix:@"Number of rows : "]) {
        NSLog(@"Response %@ in unexpected format. Cannot parse", responseString);
        return;
    }
    
    NSString *countStart = [responseString substringFromIndex:17]; // to account for "Number of rows : "
    
    int count, i = 0;
    NSMutableString *countStr = [[NSMutableString alloc] init];
    for (i = 0; i < [countStart length]; i++) {
        if ([countStart characterAtIndex:i] == ' ') {
            break;
        }
        else {
            [countStr appendFormat:@"%C", [countStart characterAtIndex:i]];
        }
    }
    
    count = [countStr intValue];
    
    NSString *dataStart = [countStart substringFromIndex:(i+3)]; // to account for "- "
    
    NSArray *dataParts = [dataStart componentsSeparatedByString:@";"];
    NSMutableArray *searchEntries = [NSMutableArray array];
    for (NSString *dataPart in dataParts) {
        
        //
        // Last object is an empty string placeholder.
        //
        if (dataPart == [dataParts lastObject]) {
            continue;
        }
        
        RBSearchEntry *entry = [[RBSearchEntry alloc] initWithSearchEntry:dataPart];
        if (entry != nil) {
            [searchEntries addObject:entry];
        }
    }
    
    _searchResults = searchEntries;
    [self performSelectorOnMainThread:@selector(onSearchComplete) withObject:nil waitUntilDone:NO];
}

- (void) onSearchComplete
{
    [_listener onSearchComplete];
}

#pragma mark Result count

- (NSUInteger) getSearchResultCount
{
    if (_searchResults == nil) return 0;
    
    return _searchResults.count;
}

#pragma mark Entry lookup methods

- (RBSearchEntry *) entryAtIndex:(NSUInteger)index
{
    if (index > [_searchResults count]) {
        return nil;
    }
    
    return [_searchResults objectAtIndex:index];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_searchResults count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BhajanListCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BhajanListCell"];
    }
    
    RBSearchEntry *entry = [_searchResults objectAtIndex:indexPath.row];
    cell.textLabel.text = [entry title];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.accessoryView = [self favoriteToggleButton:entry indexPath:indexPath];
    
    return cell;
}

- (UIButton*) favoriteToggleButton:(RBSearchEntry*)entry indexPath:(NSIndexPath*)indexPath;
{
    UIButton *button = [[UIButton alloc] init];
    button.tag = 9000 + indexPath.row;
    if ([_listener isFavorite:entry]) {
        [button setImage:[UIImage imageNamed:@"favoriteOff.png"] forState:UIControlStateNormal];
    }
    else {
        [button setImage:[UIImage imageNamed:@"favoriteOn.png"] forState:UIControlStateNormal];
    }
    
    button.frame = CGRectMake(0, 0, 40, 40);
    
    [button addTarget:self action:@selector(toggleFavoriteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (void) toggleFavoriteButtonPressed:(UIButton*)button
{
    RBSearchEntry *entry = [_searchResults objectAtIndex:(button.tag - 9000)];
    if ([_listener isFavorite:entry]) {
        [_listener removeFavorite:entry];
        [button setImage:[UIImage imageNamed:@"favoriteOn.png"] forState:UIControlStateNormal];
    }
    else {
        [_listener addFavorite:entry];
        [button setImage:[UIImage imageNamed:@"favoriteOff.png"] forState:UIControlStateNormal];
    }
}

@end
