//
//  RBConsts.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TINT_COLOR [UIColor colorWithRed:23/255.0f green:186/255.0f blue:255/255.0f alpha:1.0f]

@interface RBConsts : NSObject

@end
