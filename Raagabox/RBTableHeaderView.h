//
//  RBTableHeaderView.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 12/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RBTableHeaderView;

@protocol RBTableHeaderViewDelegate <NSObject>

- (void) playAllButtonPressed;

@end

@interface RBTableHeaderView : UIView

- (id) initWithFrame:(CGRect)frame delegate:(id<RBTableHeaderViewDelegate>)delegate;

- (void) setResultCount:(NSUInteger)resultCount;

@end
