//
//  RBViewController.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "RBAudioPlayerView.h"
#import "RBTableHeaderView.h"
#import "RBDataSourceListener.h"

@interface RBViewController : UIViewController<UITableViewDelegate,
                                               UIActionSheetDelegate,
                                               UISearchBarDelegate,
                                               MFMailComposeViewControllerDelegate,
                                               RBTableHeaderViewDelegate,
                                               RBDataSourceListener,
                                               RBAudioPlayerViewDataSource>

@end
