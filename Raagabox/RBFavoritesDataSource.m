//
//  RBFavoritesDataSource.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 02/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import "RBFavoritesDataSource.h"

@interface RBFavoritesDataSource()

@property (nonatomic, assign) id<RBDataSourceListener> listener;
@property (nonatomic, strong) NSArray                  *searchResults;
@property (nonatomic, strong) NSMutableDictionary      *favorites;

@end

@implementation RBFavoritesDataSource

static NSString *kFavouriteItemsKey = @"raagaboxFavorites";

# pragma mark Initialization

- (id) initWithListener:(id<RBDataSourceListener>)listener
{
    self = [super init];
    if (self) {
        _listener      = listener;
        _favorites     = [NSMutableDictionary dictionary];
        [self loadFavorites];
       _searchResults = [self allFavorites];
    }
    
    return self;
}

- (NSArray*) allFavorites
{
    NSMutableArray *allFavorites = [NSMutableArray array];
    
    for (NSString *key in [_favorites allKeys]) {
        NSString *value = [_favorites objectForKey:key];
        RBSearchEntry *entry = [[RBSearchEntry alloc] initWithTitle:value filePath:key];
        [allFavorites addObject:entry];
    }
    
    return allFavorites;
}

#pragma mark Search methods

- (void) searchForKeyword:(NSString*)keyword
{
    // Handle empty or nil strings.
    if (keyword == nil || keyword.length == 0) {
        _searchResults = [self allFavorites];
        [self performSelectorOnMainThread:@selector(onSearchComplete) withObject:nil waitUntilDone:NO];
        return;
    }
    
    NSArray *keywordParts = [keyword componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // Search in background thread to avoid blocking UI.
    [self performSelectorInBackground:@selector(doSearch:) withObject:keywordParts];
}

- (void) doSearch:(NSArray*)keywords
{
    NSMutableArray *results = [NSMutableArray array];
    
    for (NSString *key in [_favorites allKeys]) {
        
        NSString *value = [_favorites objectForKey:key];
        
        BOOL match = YES;
        for (NSString *keyword in keywords) {
            if ([value rangeOfString:keyword].location == NSNotFound) {
                match = NO;
                break;
            }
        }
        
        if (match) {
            RBSearchEntry *entry = [[RBSearchEntry alloc] initWithTitle:value filePath:key];
            [results addObject:entry];
        }
    }
    
    _searchResults = results;
    [self performSelectorOnMainThread:@selector(onSearchComplete) withObject:nil waitUntilDone:NO];
}

- (void) onSearchComplete
{
    [_listener onSearchComplete];
}

#pragma mark Favorite query/add/remove methods

- (BOOL) isFavorite:(RBSearchEntry*)entry
{
    return ([_favorites objectForKey:entry.filePath] != nil);
}

- (void) addFavorite:(RBSearchEntry*)entry
{
    [_favorites setObject:entry.title forKey:entry.filePath];
    [self saveFavorites];
}

- (void) removeFavorite:(RBSearchEntry*)entry
{
    [_favorites removeObjectForKey:entry.filePath];
    [self saveFavorites];
}

- (void) loadFavorites
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:kFavouriteItemsKey];
    NSDictionary *favorites = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [_favorites removeAllObjects];
    
    if (favorites != nil) {
        [_favorites addEntriesFromDictionary:favorites];
    }
}

- (void) saveFavorites
{
    NSData *newdata = [NSKeyedArchiver archivedDataWithRootObject:_favorites];
    
    [[NSUserDefaults standardUserDefaults] setObject:newdata forKey:kFavouriteItemsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark Result count

- (NSUInteger) getSearchResultCount
{
    if (_searchResults == nil) return 0;
    
    return _searchResults.count;
}

#pragma mark Entry lookup methods

- (RBSearchEntry *) entryAtIndex:(NSUInteger)index
{
    if (index > [_searchResults count]) {
        return nil;
    }
    
    return [_searchResults objectAtIndex:index];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_searchResults count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BhajanListCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BhajanListCell"];
    }
    
    RBSearchEntry *entry = [_searchResults objectAtIndex:indexPath.row];
    cell.textLabel.text = [entry title];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.accessoryView = [self removeFavoriteButton:entry indexPath:indexPath];
    
    return cell;
}

- (UIButton*) removeFavoriteButton:(RBSearchEntry*)entry indexPath:(NSIndexPath*)indexPath;
{
    UIButton *button = [[UIButton alloc] init];
    button.tag = 9000 + indexPath.row;
    [button setImage:[UIImage imageNamed:@"favoriteOff.png"] forState:UIControlStateNormal];
    
    button.frame = CGRectMake(0, 0, 40, 40);
    
    [button addTarget:self action:@selector(removeFavoriteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (void) removeFavoriteButtonPressed:(UIButton*)button
{
    RBSearchEntry *entry = [_searchResults objectAtIndex:(button.tag - 9000)];
    [self removeFavorite:entry];
    [button setImage:[UIImage imageNamed:@"favoriteOn.png"] forState:UIControlStateNormal];
    
    [self searchForKeyword:nil];
}

@end

