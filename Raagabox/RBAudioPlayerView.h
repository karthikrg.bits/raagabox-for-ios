//
//  RBAudioPlayerView.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RBAudioPlayerView;

@protocol RBAudioPlayerViewDataSource <NSObject>

- (NSString*) getPlayFilePath;
- (BOOL)      isPlayAllEnabled;
- (void)      moveToNext;
- (void)      moveToPrev;

@end

@interface RBAudioPlayerView : UIView

// Init
- (id) initWithFrame:(CGRect)frame dataSource:(id<RBAudioPlayerViewDataSource>)dataSource;

// Play
- (void) play;

// Stop
- (void) stop;

// Is playing?
- (BOOL) isPlaying;

// Is repeat enabled?
- (BOOL) isRepeatEnabled;

// Disable controls
- (void) disableControls;

// Enable controls
- (void) enableControls;

@end
