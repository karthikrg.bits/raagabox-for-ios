//
//  RBFavoritesDataSource.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 02/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBSearchEntry.h"
#import "RBDataSourceListener.h"

@interface RBFavoritesDataSource : NSObject<UITableViewDataSource>

- (id) initWithListener:(id<RBDataSourceListener>)listener;
- (void) searchForKeyword:(NSString*)keyword;
- (RBSearchEntry*) entryAtIndex:(NSUInteger)index;

- (BOOL) isFavorite:(RBSearchEntry*)entry;
- (void) addFavorite:(RBSearchEntry*)entry;
- (void) removeFavorite:(RBSearchEntry*)entry;

@end
