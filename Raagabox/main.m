//
//  main.m
//  Raagabox
//
//  Created by Karthik Ramgopal on 28/09/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RBAppDelegate class]));
    }
}
