//
//  RBMainDataSource.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 02/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBSearchEntry.h"
#import "RBDataSourceListener.h"

@interface RBMainDataSource : NSObject<UITableViewDataSource>

- (id) initWithListener:(id<RBDataSourceListener>)listener;
- (void) searchForKeyword:(NSString*)keyword;

- (NSUInteger) getSearchResultCount;
- (RBSearchEntry*) entryAtIndex:(NSUInteger)index;

@end
