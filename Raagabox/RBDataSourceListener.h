//
//  RBDataSourceListener.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 12/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBSearchEntry.h"

@protocol RBDataSourceListener <NSObject>

- (void) onSearchComplete;

- (BOOL) isFavorite:(RBSearchEntry*)entry;
- (void) addFavorite:(RBSearchEntry*)entry;
- (void) removeFavorite:(RBSearchEntry*)entry;

@end
