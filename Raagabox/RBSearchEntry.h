//
//  RBSearchEntry.h
//  Raagabox
//
//  Created by Karthik Ramgopal on 02/10/13.
//  Copyright (c) 2013 HiMahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBSearchEntry : NSObject

- (id) initWithSearchEntry:(NSString*)searchEntry;
- (id) initWithTitle:(NSString*)title filePath:(NSString*)filePath;

- (NSString*) title;
- (NSString*) filePath;


@end
